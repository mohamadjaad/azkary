package com.hashstudio.azkary.DetailsActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hashstudio.azkary.Adapters.ReplaceFont;
import com.hashstudio.azkary.R;


public class DetailsFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "DetailsFragment";
    TextView tv_description, hintTv, counterTv, counter_numTv, currentZekr , alzekr , mn ;
    RelativeLayout layout ;
    Button counterButton ;
    long counter_num;
    int page ;
    boolean move , vibrate , sound ;

    public DetailsFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_details, container, false);
        tv_description = view.findViewById(R.id.description);
        hintTv = view.findViewById(R.id.hint);
        counterTv = view.findViewById(R.id.counterTextView);
        counter_numTv = view.findViewById(R.id.counterNumTextView);
        currentZekr = view.findViewById(R.id.currentZekr);
        layout = view.findViewById(R.id.detailsLayout);
        counterButton = view.findViewById(R.id.counterButton);
        alzekr = view.findViewById(R.id.alzekrWord);
        mn = view.findViewById(R.id.menWord);


        getData();
        setFont();
        checkSetting();

        tv_description.setOnClickListener(this);
        hintTv.setOnClickListener(this);
        counterButton.setOnClickListener(this);

        return view;
    }

    private void getData() {

        String description = getArguments().getString("description");
        String hint = getArguments().getString("hint");
        String counter = getArguments().getString("counter");
        counter_num = getArguments().getLong("counter_num");

        page = getArguments().getInt("page");
        int pages_num = getArguments().getInt("pages_num");

        Log.d(TAG, "getData: " + description + hint );
        System.out.println("counter  " + counter);
        System.out.println("counter_num   " + counter_num);
        System.out.println("page  " + page);
        System.out.println("pages_num  " + pages_num);

        tv_description.setText(description);
        hintTv.setText(hint);
        if (counter != null) {
            counterTv.setText(counter);
        }
        counter_numTv.setText(String.valueOf(pages_num));
        currentZekr.setText(String.valueOf(page));
    }

    @Override
    public void onClick(View v) {

        if (sound){
            MediaPlayer mediaPlayer = MediaPlayer.create(getContext() , R.raw.counter_sound );
            mediaPlayer.start();
        }

        long i = Long.parseLong( counterButton.getText().toString() );

        if (counter_num > i ){

            counterButton.setText( String.valueOf( i + 1 ) );

            i++;
        }

        if (counter_num == i && move ){

            // TODO: 2/11/2019 vibrate when moving to next fragment

            if (vibrate){
                Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);

                vibrator.vibrate(300);
            }

            ( (ZekrDetails)getActivity()).swipe( page  , true );
        }
    }

    private void setFont(){

        SharedPreferences sharedPref = getActivity().getSharedPreferences("fontPref" , Context.MODE_PRIVATE);
        String font = sharedPref.getString("chosenFont" ,"font/a.otf");
        Log.d(TAG, "onCreateView: " + font );
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), font);
        tv_description.setTypeface(typeface);
        hintTv.setTypeface(typeface);
        counterTv.setTypeface(typeface);
        counter_numTv.setTypeface(typeface);
        currentZekr.setTypeface(typeface);
        //alzekr.setTypeface(typeface);
        mn.setTypeface(typeface);

        //fontSize
        int fontSize = sharedPref.getInt("fontSize" , 16);
        tv_description.setTextSize(fontSize  );
        hintTv.setTextSize(fontSize );

    }

    private void checkSetting(){
        SharedPreferences sharedPref = getActivity().getSharedPreferences("fontPref" , Context.MODE_PRIVATE);

        move = sharedPref.getBoolean("moveSwitch" , true );
        vibrate = sharedPref.getBoolean("vibrationSwitch" , true );
        sound = sharedPref.getBoolean("soundSwitch" , true );

        Log.d(TAG, "checkSetting: " + move +"\n" + vibrate + "\n"  + sound );
    }

}


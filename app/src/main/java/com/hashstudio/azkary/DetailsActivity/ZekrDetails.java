package com.hashstudio.azkary.DetailsActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.duolingo.open.rtlviewpager.RtlViewPager;
import com.hashstudio.azkary.Adapters.FragmentAdapter;
import com.hashstudio.azkary.Adapters.ZoomOutPageTransformer;
import com.hashstudio.azkary.R;
import com.hashstudio.azkary.db.AzkaryContract;
import com.hashstudio.azkary.db.DbAccess;
import com.hashstudio.azkary.db.DbHelper;
import com.hashstudio.azkary.model.Category;
import com.hashstudio.azkary.model.Zekr;
import java.util.List;

public class ZekrDetails extends AppCompatActivity {


    private static final String TAG = "ZekrDetails";

    ImageButton back;

    FragmentAdapter adapter;
    long category_id;
    String title = "";
    TextView titleTextView;
    DbAccess databaseAccess;
    DbHelper helper;
    SQLiteDatabase database;
    boolean bookmarked;
    RtlViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zekr_details);
        initWidgets();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getDataLists();
        setupViewPager();

    }

    public void getDataLists() {

        Intent i = getIntent();
        category_id = i.getLongExtra("id", 0);
        title = i.getStringExtra("title");
        titleTextView.setText(title);

        Log.d(TAG, "getDataLists: title  " + title + "  id  " + category_id);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.zekr_options, menu);

        checkBookmark();
        if (bookmarked) {
            menu.findItem(R.id.bookmark).setTitle("ازالة من المفضلة ");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.copy:
                // TODO: 2/12/2019  copy text to clipboard
                copy();
                break;

            case R.id.share:
                // TODO: 2/12/2019  share zekr to facebook , whatsApp , ...etc
                share();
                break;

            case R.id.bookmark:
                //update database with bookmark location
                if (!bookmarked) {
                    addBookmark();
                } else if (bookmarked) {

                    removeBookmark();
                }
                break;
        }
        return true;
    }

    private void initWidgets() {

        back = findViewById(R.id.backButton);

        viewPager = findViewById(R.id.viewpager);
        titleTextView = findViewById(R.id.titleText);

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        helper = new DbHelper(getApplicationContext());
        database = helper.getWritableDatabase();

    }

    private void setupViewPager() {

        adapter = new FragmentAdapter(getSupportFragmentManager(), getZekrList(category_id));
        viewPager.setPageTransformer(true  , new ZoomOutPageTransformer());

        viewPager.setLayoutDirection(ViewPager.LAYOUT_DIRECTION_RTL);

        viewPager.setAdapter(adapter);

    }

    private List<Zekr> getZekrList(long category_id) {

        databaseAccess = DbAccess.getInstance(this);
        databaseAccess.open();
        List<Zekr> zekrs = databaseAccess.getZekr(category_id);
        databaseAccess.close();

        return zekrs;

    }

    private void addBookmark() {
        Log.d(TAG, "addBookmark: ");

        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("bookmarked", 1);

        int count = database.update(AzkaryContract.Entries.TABLE_CATEGORY,
                values,
                AzkaryContract.Entries.KEY_ID + "=?",
                new String[]{String.valueOf(category_id)});

        System.out.println("rows updated " + count);
        Toast.makeText(this, "تمت الاضافة الى الاذكار المفضلة", Toast.LENGTH_SHORT).show();


    }

    private void removeBookmark() {
        Log.d(TAG, "removeBookmark: ");

        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        //values.put("id" , category_id );
        //values.put("name" , a );
        values.put("bookmarked", 0);

        int count = database.update(AzkaryContract.Entries.TABLE_CATEGORY,
                values,
                AzkaryContract.Entries.KEY_ID + "=?",
                new String[]{String.valueOf(category_id)});

        System.out.println("rows updated " + count);
        Toast.makeText(this, " تمت الازالة من المفضلة ", Toast.LENGTH_SHORT).show();

    }

    private void checkBookmark() {

        Log.d(TAG, "checkBookmark: ");

        Cursor cursor = database.rawQuery("select bookmarked from category where id = " + category_id,
                null);
        int i = 0;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            i = cursor.getInt(0);
            cursor.moveToNext();
        }
        cursor.close();
        System.out.println(cursor);

        if (i == 1) {
            bookmarked = true;
        } else
            bookmarked = false;


    }

    public void swipe(int item , boolean smoothScroll){
        viewPager.setCurrentItem(item , smoothScroll);

    }

    private void copy(){

        String description = getZekrList(category_id).get(viewPager.getCurrentItem()).getDescription();
        String hint = getZekrList(category_id).get(viewPager.getCurrentItem()).getHint();

        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText( "label" , " من " + title + "\n"+ description + "\n" + hint );
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }

        System.out.println( " من "+ title + "\n"+ description + "\n" + hint );
        Toast.makeText(this, "تم النسخ", Toast.LENGTH_SHORT).show();

    }

    private void share(){

        String description = getZekrList(category_id).get(viewPager.getCurrentItem()).getDescription();
        String hint = getZekrList(category_id).get(viewPager.getCurrentItem()).getHint();

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, " من " + title + "\n"+ description + "\n" + hint);
        sendIntent.setType("text/plain");
        //sendIntent.setPackage("com.whatsapp");
        startActivity(sendIntent);

    }
}

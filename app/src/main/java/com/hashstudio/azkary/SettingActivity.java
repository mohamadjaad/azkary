package com.hashstudio.azkary;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.hashstudio.azkary.Adapters.ReplaceFont;

public class SettingActivity extends AppCompatActivity {

    private static final String TAG = "SettingActivity";
    Switch moveSwitch , vibrationSwitch , soundSwitch ;
    TextView tvTest ;
    SeekBar sizeSeekBar ;
    Spinner fontSpinner ;
    SharedPreferences preferences ;
    SharedPreferences.Editor editor ;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Log.d(TAG, "onCreate:  ");

        initWidgets();
        setFontSpinner();
        setSizeSeekBar();
        switches();

    }

    private void initWidgets(){

        moveSwitch = findViewById(R.id.moveSwitch);
        vibrationSwitch = findViewById(R.id.vibrationSwitch);
        soundSwitch = findViewById(R.id.soundSwitch);
        tvTest = findViewById(R.id.testTextView);
        sizeSeekBar = findViewById(R.id.textSizeSeekbar);
        fontSpinner = findViewById(R.id.fontTypeSpinner);
        preferences =getSharedPreferences("fontPref" , MODE_PRIVATE );
        editor = preferences.edit();

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setFontSpinner(){

        int selection = preferences.getInt("selectedItem", 0 );

        Log.d(TAG, "setFontSpinner: selection " + selection );
        String[] fonts = getResources().getStringArray(R.array.fontTypes);

        ArrayAdapter adapter = new ArrayAdapter(getApplicationContext() ,
                android.R.layout.simple_spinner_dropdown_item , fonts );

        fontSpinner.setAdapter(adapter);

        fontSpinner.setSelection( selection );

        fontSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){

                    case 0 :
                        Typeface typeface = Typeface.createFromAsset(getAssets() , "font/a.otf");
                        tvTest.setTypeface(typeface);

                        ReplaceFont.changeDefaultFont(getApplication(), "DEFAULT", "font/a.otf");
                        editor.putString("chosenFont" , "font/a.otf" );
                        editor.putInt("selectedItem" , 0);
                        editor.commit();
                        break;

                    case 1 :
                        Typeface typeface1 = Typeface.createFromAsset(getAssets() , "font/b.otf");
                        tvTest.setTypeface(typeface1);

                        editor.putString("chosenFont" , "font/b.otf" );
                        editor.putInt("selectedItem" , 1);
                        editor.commit();
                        break;

                    case 2 :
                        Typeface typeface2 = Typeface.createFromAsset(getAssets() , "font/c.otf");;
                        tvTest.setTypeface(typeface2);

                        editor.putString("chosenFont" , "font/c.otf" );
                        editor.putInt("selectedItem" , 2);
                        editor.commit();
                        break;

                    case 3 :
                        Typeface typeface3 = Typeface.createFromAsset(getAssets() , "font/d.otf");;
                        tvTest.setTypeface(typeface3);

                        editor.putString("chosenFont" , "font/d.otf" );
                        editor.putInt("selectedItem" , 3);
                        editor.commit();
                        break;

                    case 4 :
                        Typeface typeface4 = Typeface.createFromAsset(getAssets() , "font/e.otf");;
                        tvTest.setTypeface(typeface4);

                        editor.putString("chosenFont" , "font/e.otf" );
                        editor.putInt("selectedItem" , 4);
                        editor.commit();
                        break;

                    case 5 :
                        Typeface typeface5 = Typeface.createFromAsset(getAssets() , "font/f.otf");
                        tvTest.setTypeface(typeface5);

                        editor.putString("chosenFont" , "font/f.otf" );
                        editor.putInt("selectedItem" , 5);
                        editor.commit();

                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setSizeSeekBar(){

        int progress  = preferences.getInt("fontSize" , 22);
        sizeSeekBar.setMax(25);
        sizeSeekBar.setProgress(progress);
        sizeSeekBar.setKeyProgressIncrement(2);
        tvTest.setTextSize(progress+10);
        sizeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvTest.setTextSize(progress + 10 );
                editor.putInt("fontSize" , progress);
                editor.commit();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void switches(){

        soundSwitch.setChecked(preferences.getBoolean("soundSwitch" ,true) );

        soundSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putBoolean("soundSwitch", soundSwitch.isChecked());
                editor.commit();
            }
        });


        moveSwitch.setChecked(preferences.getBoolean("moveSwitch" ,true) );

        moveSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putBoolean("moveSwitch", moveSwitch.isChecked());
                editor.commit();
            }
        });


        vibrationSwitch.setChecked(preferences.getBoolean("vibrationSwitch" ,true) );

        vibrationSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putBoolean("vibrationSwitch", vibrationSwitch.isChecked());
                editor.commit();
            }
        });

    }
}

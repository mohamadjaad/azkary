package com.hashstudio.azkary.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class DbHelper extends SQLiteAssetHelper {
    private static final String TAG = "DbHelper";

    private static final String DATABASE_NAME = "azkary.db" ;
    private static final int DATABASE_VERSION = 1 ;

    public DbHelper( @Nullable Context context,
                     @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

}

package com.hashstudio.azkary.db;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.hashstudio.azkary.db.AzkaryContract;
import com.hashstudio.azkary.db.DbAccess;
import com.hashstudio.azkary.db.DbHelper;

import org.jetbrains.annotations.NotNull;


public class AzkaryProvider extends ContentProvider {
    private static final String TAG = "AzkaryProvider";
    DbHelper helper ;

    // Creates a UriMatcher object.
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        /*
         * The calls to addURI() go here, for all of the content URI patterns that the provider
         * should recognize. For this snippet, only the calls for table c are shown.
         */

        /*
         * Sets the integer value for multiple rows in table (category) to a. Notice that no wildcard is used
         * in the path
         */
        uriMatcher.addURI(AzkaryContract.CONTENT_AUTHORITY, AzkaryContract.PATH_azkary, 100);

        /*
         * Sets the code for a single row to b. In this case, the "#" wildcard is
         * used. "content://com.hashstudio.azkary/category/c" matches, but
         * "content://com.hashstudio.azkary/category doesn't.
         */
        uriMatcher.addURI(AzkaryContract.CONTENT_AUTHORITY, AzkaryContract.PATH_azkary + "/#", 101);
    }

    @Override
    public boolean onCreate() {
        helper = new DbHelper(getContext());
        return true;
    }


    @Override
    public Cursor query(@NotNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        SQLiteDatabase database = helper.getReadableDatabase();
        Cursor cursor = null ;
        int match = uriMatcher.match(uri);
        Log.d(TAG, "query: ");
        switch (match){

            case 100 :
                cursor = database.query(AzkaryContract.Entries.TABLE_CATEGORY , projection ,
                        selection , selectionArgs ,null , null ,sortOrder);
                break;

            case 101 :

                selection = AzkaryContract.Entries.CATEGORY_ID + "=?" ;
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};

                cursor = database.query(AzkaryContract.Entries.TABLE_CATEGORY ,projection ,
                        selection , selectionArgs , null ,null , sortOrder );
                break;

            default:
                throw new IllegalArgumentException("Unknown uri" + uri );
        }

        cursor.setNotificationUri(getContext().getContentResolver() , uri);

        return cursor ;
    }


    @Override
    public String getType(@NotNull Uri uri) {
        final int match = uriMatcher.match(uri);
        switch (match){

            case 100 :
                return ContentResolver.CURSOR_DIR_BASE_TYPE +"/" + AzkaryContract.CONTENT_AUTHORITY +
                        "/" + AzkaryContract.PATH_azkary ;

            case  101 : return ContentResolver.ANY_CURSOR_ITEM_TYPE +"/" + AzkaryContract.CONTENT_AUTHORITY +
                    "/" + AzkaryContract.PATH_azkary ;

                default:
                    throw new UnsupportedOperationException("Unknown Uri" + uri );
        }
    }


    @Override
    public Uri insert(@NotNull Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NotNull Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }


    @Override
    public int update(@NotNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        SQLiteDatabase db = helper.getWritableDatabase();

        int rowsUpdated = db.update(AzkaryContract.Entries.TABLE_CATEGORY, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);

        return rowsUpdated;
    }


}

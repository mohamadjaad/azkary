package com.hashstudio.azkary.db;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class AzkaryContract {

    private AzkaryContract(){
    }

    public static final String CONTENT_AUTHORITY = "com.hashstudio.azkary.db";
    public static final String PATH_azkary = "azkary";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);


    public static class Entries implements BaseColumns {

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_azkary);

        /**
         * The MIME type of the {@link #CONTENT_URI} for a list of pets.
         */
        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_azkary;

        /**
         * The MIME type of the {@link #CONTENT_URI} for a single pet.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_azkary;



        public static final String TABLE_CATEGORY = "category";
        public static final String TABLE_ZEKR = "zekr";

        //common column names
        public static final String KEY_ID = "id";

        //category table column names
        public static final String NAME = "name";
        public static final String FAVORITES = "bookmarked";

        //zekr table column names
        public static final String CATEGORY_ID = "category_id";
        public static final String DESCRIPTION = "description";
        public static final String HINT = "hint";
        public static final String COUNTER = "counter";
        public static final String COUNTER_NUM = "counter_num";








        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_SUBTITLE = "subtitle";
    }

}

package com.hashstudio.azkary.db;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.hashstudio.azkary.model.Category;
import com.hashstudio.azkary.model.Zekr;

import java.util.ArrayList;
import java.util.List;

public class DbAccess {
    private static final String TAG = "DbAccess";

    private DbHelper openHelper;
    private SQLiteDatabase database;
    private static DbAccess instance;

    /**
     * Private constructor to avoid object creation from outside classes.
     *
     * @param context
     */
    public DbAccess(Context context) {
        this.openHelper = new DbHelper(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DbAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DbAccess(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        Log.d(TAG, "open database: ");
        this.database = openHelper.getReadableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     * Read all quotes from the database.
     *
     * @return a List of quotes
     */

    //get Categories to mainActivity
    public List<Category> getCategories() {

        List<Category> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM category", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Category category = new Category();
            category.setId(cursor.getLong(cursor.getColumnIndex("id")));
            category.setName(cursor.getString(cursor.getColumnIndex("name")));
            category.setBookmarked(cursor.getInt(cursor.getColumnIndex("bookmarked")));
            list.add(category);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public List<Zekr> getZekr(long category_id) {

        List<Zekr> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM zekr " + "where  category_id = " + category_id, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Zekr zekr = new Zekr();
            zekr.setId(cursor.getLong(cursor.getColumnIndex("id")));
            zekr.setDescription(cursor.getString(cursor.getColumnIndex("description")));
            zekr.setHint(cursor.getString(cursor.getColumnIndex("hint")));
            zekr.setCategory_id(category_id);
            zekr.setCounter(cursor.getString(cursor.getColumnIndex("counter" )));
            zekr.setCounter_num(cursor.getLong(cursor.getColumnIndex("counter_num")));
            list.add(zekr);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

/*

    public List<Integer> getBookmarkedCategories() {

        List<Integer> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT bookmarked FROM category ", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getInt(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public List<String> getAzkarDescription(long category_id) {
        Log.d(TAG, "getAzkarDescription: id =  " + category_id);
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT description FROM zekr " +
                        "where  category_id = " + category_id
                , null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        Log.d(TAG, "getAzkarDescription: " + list);
        return list;
    }

    public List<String> getAzkarHint(long category_id) {
        Log.d(TAG, "getAzkarHint: id =  " + category_id);
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT hint FROM zekr " +
                        "where  category_id = " + category_id
                , null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        Log.d(TAG, "getAzkarHint: " + list);
        return list;
    }

    public List<String> getAzkarCounter(long category_id) {
        Log.d(TAG, "getAzkarCounter: id =  " + category_id);
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT counter FROM zekr " +
                        "where  category_id = " + category_id
                , null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        Log.d(TAG, "getAzkarCounter: " + list);
        return list;
    }

    public List<String> getAzkarCounterNum(long category_id) {
        Log.d(TAG, "getAzkarCounterNumber: id =  " + category_id);
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT counter_num FROM zekr " +
                        "where  category_id = " + category_id
                , null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        Log.d(TAG, "getAzkarCounterNumber: " + list);
        return list;
    }

    public List<String> getBookmarkString() {

        Log.d(TAG, "getBookmarkString:  =  ");

        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT name FROM category " + " where  bookmarked = " + a
                , null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        Log.d(TAG, "getBookmarkString: " + list);
        return list;
    }

    public List<Integer> getBookmarksIndex() {

        List<Integer> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT bookmarked FROM category " + " where  bookmarked = " + a, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getInt(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

*/

}


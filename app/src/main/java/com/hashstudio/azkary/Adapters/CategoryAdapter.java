package com.hashstudio.azkary.Adapters;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hashstudio.azkary.R;
import com.hashstudio.azkary.DetailsActivity.ZekrDetails;
import com.hashstudio.azkary.db.AzkaryContract;
import com.hashstudio.azkary.db.DbHelper;
import com.hashstudio.azkary.model.Category;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private static final String TAG = "CategoryAdapter";

    private List<Category> categoryList ;
    private Context context;

    public CategoryAdapter(Context context, List<Category> categoryList) {
        Log.d(TAG, "CategoryAdapter: ");
        this.context = context;
        this.categoryList = categoryList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        Log.d(TAG, "onCreateViewHolder: called");

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_list_item, parent, false);

        ViewHolder holder = new ViewHolder(view);

        return holder;

    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.ViewHolder holder, final int i) {
        Log.d(TAG, "onBindViewHolder: called  ");

        holder.textView.setText(categoryList.get(i).getName());

        if (categoryList.get(i).getBookmarked() == 1) {
            holder.imageView.setImageResource(R.mipmap.star_full);

        } else {
            holder.imageView.setImageResource(R.mipmap.star_empty);

        }
        SharedPreferences sharedPref = context.getSharedPreferences("fontPref" , Context.MODE_PRIVATE);
        String font = sharedPref.getString("chosenFont" ,"font/a.otf");
        int size = sharedPref.getInt("fontSize" , 16 );
        Typeface typeface = Typeface.createFromAsset(context.getAssets() , font );
        holder.textView.setTypeface(typeface);
        holder.textView.setTextSize(size);

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ZekrDetails.class);
                intent.putExtra("id", categoryList.get(i).getId());
                intent.putExtra("title", categoryList.get(i).getName());
                context.startActivity(intent);
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: imageview clicked ");
                if (categoryList.get(i).getBookmarked() == 0 ){
                    //item not bookmarked we have to add a bookmark
                    addBookmark(categoryList.get(i).getId());
                    categoryList.get(i).setBookmarked(1);

                    notifyDataSetChanged();

                }else {
                    //item is bookmarked we need to remove bookmark

                    removeBookmark(categoryList.get(i).getId());
                    categoryList.get(i).setBookmarked(0);

                    notifyDataSetChanged();
                }

            }
        });

    }


    @Override
    public int getItemCount() {
        return categoryList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        LinearLayout layout;
        TextView textView;
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            layout = itemView.findViewById(R.id.layout);
            textView = itemView.findViewById(R.id.categoryItem);
            imageView = itemView.findViewById(R.id.bookmarkIcon);

        }
    }

    private void addBookmark(long id) {

        Log.d(TAG, "addBookmark: ");
        DbHelper helper =new DbHelper(context) ;
        SQLiteDatabase database = helper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("bookmarked", 1);

        int count = database.update(AzkaryContract.Entries.TABLE_CATEGORY,
                values,
                AzkaryContract.Entries.KEY_ID + "=?",
                new String[]{String.valueOf(id)});

        System.out.println("rows updated " + count);
        Toast.makeText(context , "تمت الاضافة الى الاذكار المفضلة", Toast.LENGTH_SHORT).show();

    }

    private void removeBookmark(long id) {
        Log.d(TAG, "removeBookmark: ");

        DbHelper helper =new DbHelper(context) ;
        SQLiteDatabase database = helper.getWritableDatabase();

        ContentValues values = new ContentValues();
        //values.put("id" , category_id );
        //values.put("name" , a );
        values.put("bookmarked", 0);

        int count = database.update(AzkaryContract.Entries.TABLE_CATEGORY,
                values,
                AzkaryContract.Entries.KEY_ID + "=?",
                new String[]{String.valueOf(id)});

        System.out.println("rows updated " + count);
        Toast.makeText(context , " تمت الازالة من المفضلة ", Toast.LENGTH_SHORT).show();

    }
}

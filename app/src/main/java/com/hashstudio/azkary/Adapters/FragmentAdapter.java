package com.hashstudio.azkary.Adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.hashstudio.azkary.DetailsActivity.DetailsFragment;
import com.hashstudio.azkary.model.Zekr;
import java.util.ArrayList;
import java.util.List;

public class FragmentAdapter extends FragmentPagerAdapter {


    List<Zekr> data = new ArrayList<>();

    public FragmentAdapter(FragmentManager fm, List<Zekr> data) {
        super(fm);
        this.data = data;
    }


    @Override
    public Fragment getItem(int i) {


        Fragment fragment = new DetailsFragment();
        Bundle bundle = new Bundle();

        bundle.putString("description", data.get(i).getDescription());
        bundle.putString("hint" , data.get(i).getHint());
        bundle.putString("counter" , data.get(i).getCounter());
        bundle.putLong("counter_num" , data.get(i).getCounter_num());
        bundle.putInt("page" , i + 1 );
        bundle.putInt("pages_num" , data.size() );

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return data.size();
    }
}

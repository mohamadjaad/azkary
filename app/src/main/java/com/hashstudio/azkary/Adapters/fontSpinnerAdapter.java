package com.hashstudio.azkary.Adapters;

import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;

import java.util.ArrayList;
import java.util.List;


public class fontSpinnerAdapter implements SpinnerAdapter {

    private static final String TAG = "fontSpinnerAdapter";
    List<String>  items = new ArrayList<>();
    List<Typeface> fonts = new ArrayList<>();

    public fontSpinnerAdapter(List<String> items, List<Typeface> fonts) {
        this.items = items;
        this.fonts = fonts;
    }

    public fontSpinnerAdapter() {
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return null;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return items.size() ;
    }

    @Override
    public Object getItem(int position) {

        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}

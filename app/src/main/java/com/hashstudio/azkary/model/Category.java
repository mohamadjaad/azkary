package com.hashstudio.azkary.model;

public class Category {
    long id;
    int bookmarked;
    String name , name_abstract;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getBookmarked() {
        return bookmarked;
    }

    public void setBookmarked(int bookmarked) {
        this.bookmarked = bookmarked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName_abstract() {
        return name_abstract;
    }

    public void setName_abstract(String name_abstract) {
        this.name_abstract = name_abstract;
    }
}

package com.hashstudio.azkary;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.hashstudio.azkary.Adapters.CategoryAdapter;
import com.hashstudio.azkary.Adapters.ReplaceFont;
import com.hashstudio.azkary.db.DbAccess;
import com.hashstudio.azkary.db.DbHelper;
import com.hashstudio.azkary.model.Category;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView categoryList;
    private static final String TAG = "MainActivity";
    DbAccess databaseAccess;
    CategoryAdapter adapter;
    android.support.v7.widget.SearchView searchView;
    List<Category> categories;
    List<Category> searchList = null ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initWidgets();

        //search view
        searchView.setQueryHint("Search");

    }

    @Override
    protected void onStart() {
        super.onStart();

        adapter = new CategoryAdapter(this , getCategoriesList());
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        categoryList.setLayoutManager(llm);
        categoryList.setAdapter(adapter);

        // ------------ Search View  --------------

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                return false ;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                searchList = search(s);
                Log.d(TAG, "Search list :  " + searchList );

                if (searchList != null) {
                    adapter = new CategoryAdapter(getApplicationContext(), searchList);
                    categoryList.setAdapter(adapter);
                }else {

                    adapter = new CategoryAdapter(getApplicationContext(), new ArrayList<Category>());
                    categoryList.setAdapter(adapter);

                }

                return true;
            }
        });

    }

    private void initWidgets() {
        categoryList = findViewById(R.id.categoryListView);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbarMain);
        setSupportActionBar(toolbar);
        searchView = findViewById(R.id.searchButton);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.nav_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.azkar:
                adapter = new CategoryAdapter(this , getCategoriesList() );
                categoryList.setAdapter(adapter);
                break;


            case R.id.bookmark:

                adapter = new CategoryAdapter(this , getBookmarkedCategory() );
                categoryList.setAdapter(adapter);

                break;


            case R.id.setting:
                startActivity(new Intent(this, SettingActivity.class));
                break;
        }

        return true;
    }


    private List<Category> getCategoriesList() {
        databaseAccess = DbAccess.getInstance(this);
        databaseAccess.open();
        categories = databaseAccess.getCategories();
        databaseAccess.close();

        return categories;

    }

    private List<Category> getBookmarkedCategory(){

        List<Category> list = new ArrayList<>();

        for (int i = 0 ; i<getCategoriesList().size() ; i++ ){
            Category category = getCategoriesList().get(i);
            int bookmark = category.getBookmarked();

             if(bookmark == 1){
                list.add(category);
            }

        }
        return list ;
    }

    private List<Category> search(String keyword){
        List<Category> categoriesSearch = null;
        try {
            DbHelper helper = new DbHelper(getApplicationContext());
            SQLiteDatabase database = helper.getReadableDatabase();
            Cursor cursor = database.rawQuery("select * from category  where name_abstract  like ?", new String[] { "%" + keyword + "%" });

            if (cursor.moveToFirst()) {
                categoriesSearch = new ArrayList<Category>();
                do {
                    Category category = new Category();
                    category.setId(cursor.getInt(0));
                    category.setName(cursor.getString(1));
                    category.setBookmarked(cursor.getInt(3));
                    categoriesSearch.add(category);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            categoriesSearch = null;
        }
        return categoriesSearch ;
    }
}

